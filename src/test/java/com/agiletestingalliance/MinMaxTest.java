package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void testfVari() {
        MinMax minMax = new MinMax();
        assertEquals(5, minMax.fVari(3, 5));
        assertEquals(7, minMax.fVari(7, 4));
        assertEquals(0, minMax.fVari(0, -1));
    }

    @Test
    public void testbar() {
        MinMax minMax = new MinMax();
        assertEquals("hello", minMax.bar("hello"));
        assertEquals("", minMax.bar(""));
        assertNull(minMax.bar(null));
    }
}
